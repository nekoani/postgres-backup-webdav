FROM postgres:alpine

RUN set -x \
	&& apk update && apk add ca-certificates curl \
	&& curl -L https://github.com/odise/go-cron/releases/download/v0.0.7/go-cron-linux.gz | zcat > /usr/local/bin/go-cron \
	&& chmod a+x /usr/local/bin/go-cron

ENV POSTGRES_EXTRA_OPTS '-Z9'
ENV SCHEDULE '@daily'
ENV HEALTHCHECK_PORT 8080

COPY backup.sh /backup.sh

ENTRYPOINT ["/bin/sh", "-c"]
CMD ["exec /usr/local/bin/go-cron -s \"$SCHEDULE\" -p \"$HEALTHCHECK_PORT\" -- /backup.sh"]

HEALTHCHECK --interval=5m --timeout=5s \
  CMD curl -f "http://localhost:$HEALTHCHECK_PORT/" || exit 1

